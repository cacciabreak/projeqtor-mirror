��          �               ,     -     I  �   f     4  #   G     k     �     �     �     �  K   �  (     ]   4     �  2   �  A   �  �       �     �  �   �     �  #   �     �     �                 K   2  (   ~  ]   �       2     A   H   Add a picture for the user. Changes the user's password. Default selected project and choice of character used to indent lists of projects, to represent the WBS structure of projects and sub-project (cannot be a “space”, can be “none” to get flat lists). Display parameters Generic display parameter for user. Graphic interface behavior Miscellaneous Password Photo Print and Export parameters Saving parameters will retrieve the selected parameters on each connection. Selection of graphic interface behavior. Selection of printing history for detailed items and destination for printing and PDF export. User parameters User parameters are efficient even without saving. User parameters screen allows configuration of personal settings. Project-Id-Version: ProjeQtOr User guide 5.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-08 14:45-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: fr
Language-Team: fr <LL@li.org>
Plural-Forms: nplurals=2; plural=(n > 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.5.3
 Add a picture for the user. Changes the user's password. Default selected project and choice of character used to indent lists of projects, to represent the WBS structure of projects and sub-project (cannot be a “space”, can be “none” to get flat lists). Display parameters Generic display parameter for user. Graphic interface behavior Miscellaneous Password Photo Print and Export parameters Saving parameters will retrieve the selected parameters on each connection. Selection of graphic interface behavior. Selection of printing history for detailed items and destination for printing and PDF export. User parameters User parameters are efficient even without saving. User parameters screen allows configuration of personal settings. 