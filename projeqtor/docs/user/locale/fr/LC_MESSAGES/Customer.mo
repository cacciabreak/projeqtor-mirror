��    #      4              L     M     _  #   u  $   �     �     �  
   �     �  %        2     ;     I  >   [     �     �  +   �     �  r   �  L   i  9   �  %   �          %     7     I     ^     p     �  =   �  8   �  g        o     �     �  �  �     7     I  #   _  $   �     �     �  
   �     �  %   �     	     %	     3	  >   E	     �	     �	  +   �	     �	  r   �	  L   S
  9   �
  %   �
                !     3     H     Z     v  =   z  8   �  g   �     Y     j     |   **Customer name** **\* Required field** :ref:`gui-attachment-section-label` :ref:`gui-chg-history-section-label` :ref:`gui-note-section-label` :term:`Closed` :term:`Id` Code of the customer. Complete description of the customer. Customer Customer code Delay for payment Delay for payment (in days) that can be displayed in the bill. Description Field Flag to indicate that customer is archived. Full address of the customer. It can be an internal entity, into the same enterprise, or a different enterprise, or the entity of an enterprise. It is generally the owner of the project, and in many cases it is the payer. List of the contacts known in the entity of the customer. List of the projects of the customer. Other sections Section: Addreses Section: Contacts Section: Description Section: Projects Short name of the customer. Tax Tax rates that are applied to bill amounts for this customer. The Customer is the entity for which the Project is set. The customer defined here is not a person. Real persons into customer entity are called “Contacts”. Type of customer Type of customer. Unique Id for the customer. Project-Id-Version: ProjeQtOr User guide 5.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-08 14:45-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: fr
Language-Team: fr <LL@li.org>
Plural-Forms: nplurals=2; plural=(n > 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.5.3
 **Customer name** **\* Required field** :ref:`gui-attachment-section-label` :ref:`gui-chg-history-section-label` :ref:`gui-note-section-label` :term:`Closed` :term:`Id` Code of the customer. Complete description of the customer. Customer Customer code Delay for payment Delay for payment (in days) that can be displayed in the bill. Description Field Flag to indicate that customer is archived. Full address of the customer. It can be an internal entity, into the same enterprise, or a different enterprise, or the entity of an enterprise. It is generally the owner of the project, and in many cases it is the payer. List of the contacts known in the entity of the customer. List of the projects of the customer. Other sections Section: Addreses Section: Contacts Section: Description Section: Projects Short name of the customer. Tax Tax rates that are applied to bill amounts for this customer. The Customer is the entity for which the Project is set. The customer defined here is not a person. Real persons into customer entity are called “Contacts”. Type of customer Type of customer. Unique Id for the customer. 