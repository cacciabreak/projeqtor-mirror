��    %      D              l     m     v  $   �     �  
   �     �  0   �       
          (   *     S     [     v     �     �  
   �  ,   �  J   �     !  3   <     p     �     �     �  	   �  ;   �     �       1   &     X     u  
   ~  .   �  2   �     �  �       �     �  $   �     �  
   �     �  0   �     /  
   4     ?  (   N     w          �     �     �  
   �  ,   �  J   �     E	  3   `	     �	     �	     �	     �	  	   �	  ;   �	     #
     5
  1   J
     |
     �
  
   �
  .   �
  2   �
        **Name** **\* Required field** :ref:`gui-chg-history-section-label` :term:`Closed` :term:`Id` Account number Automatically calculated from other IBAN fields. Bank Bank name. Company number Company number, to be displayed on bill. Country Country code. IBAN format. Description Field Field: Account number Field: Key Flag to indicate that recipient is archived. Flag to indicate that tax is automatically set to zero for this recipient. Format depends on country. Full account number defining the BBAN account code. Full address of the recipient. Key Key code. IBAN format. Other sections Recipient Recipients are mainly defined to store billing information. Section: Addreses Section: Description Section: International Bank Account Number (IBAN) Short name of the recipient. Tax free Tax number Tax reference number, to be displayed on bill. The Recipient is the beneficiary of bill payments. Unique Id for the recipient. Project-Id-Version: ProjeQtOr User guide 5.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-08 14:45-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: fr
Language-Team: fr <LL@li.org>
Plural-Forms: nplurals=2; plural=(n > 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.5.3
 **Name** **\* Required field** :ref:`gui-chg-history-section-label` :term:`Closed` :term:`Id` Account number Automatically calculated from other IBAN fields. Bank Bank name. Company number Company number, to be displayed on bill. Country Country code. IBAN format. Description Field Field: Account number Field: Key Flag to indicate that recipient is archived. Flag to indicate that tax is automatically set to zero for this recipient. Format depends on country. Full account number defining the BBAN account code. Full address of the recipient. Key Key code. IBAN format. Other sections Recipient Recipients are mainly defined to store billing information. Section: Addreses Section: Description Section: International Bank Account Number (IBAN) Short name of the recipient. Tax free Tax number Tax reference number, to be displayed on bill. The Recipient is the beneficiary of bill payments. Unique Id for the recipient. 