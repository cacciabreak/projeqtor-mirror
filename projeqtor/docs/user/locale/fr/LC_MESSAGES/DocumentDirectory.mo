��    (      \              �     �     �  $   �     �  
   �     �               '     -     A     Q     ]     u     �  ,   �  "   �     �  H   �  T   5  B   �     �  <   �  X   !  R   z  R   �           /     @  &   H     o  &   w     �  8   �  A   �  �   .  /   �     �  =     �  C     �	     �	  $   �	     
  
   
     *
     7
     C
     V
     \
     p
     �
     �
     �
     �
  ,   �
  "   �
       H     T   d  B   �     �  <     X   P  R   �  R   �     O     ^     o  &   w     �  &   �     �  8   �  A     �   ]  /   �       =   4   **Name** **\* Required field** :ref:`gui-chg-history-section-label` :term:`Closed` :term:`Id` Default type Description Document Directory Field Field: Default type Field: Location Field: Name Field: Parent directory Field: Product Field: Project Flag to indicate that directory is archived. Folder where files will be stored. Location Location is automatically defined as “parent location” / “name”. Location is defined relatively to “document root”, defined in global parameters. Location will automatically be “parent location” / “name”. Name of the directory. Name of the parent directory to define hierarchic structure. New document in this directory will have default document type positioned to this value. New document in this directory will have default product positioned to this value. New document in this directory will have default project positioned to this value. Other sections Parent directory Product Product the directory is dedicated to. Project Project the directory is dedicated to. Section: Description The current directory is then a sub-directory of parent. The document directories define a structure for document storage. The document files (defined on document version) will be stored in the folder defined as “location” in the “document root” place. Type of document the directory is dedicated to. Unique Id for the directory. “Document root” is defined in the global parameters file. Project-Id-Version: ProjeQtOr User guide 5.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-08 14:45-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: fr
Language-Team: fr <LL@li.org>
Plural-Forms: nplurals=2; plural=(n > 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.5.3
 **Name** **\* Required field** :ref:`gui-chg-history-section-label` :term:`Closed` :term:`Id` Default type Description Document Directory Field Field: Default type Field: Location Field: Name Field: Parent directory Field: Product Field: Project Flag to indicate that directory is archived. Folder where files will be stored. Location Location is automatically defined as “parent location” / “name”. Location is defined relatively to “document root”, defined in global parameters. Location will automatically be “parent location” / “name”. Name of the directory. Name of the parent directory to define hierarchic structure. New document in this directory will have default document type positioned to this value. New document in this directory will have default product positioned to this value. New document in this directory will have default project positioned to this value. Other sections Parent directory Product Product the directory is dedicated to. Project Project the directory is dedicated to. Section: Description The current directory is then a sub-directory of parent. The document directories define a structure for document storage. The document files (defined on document version) will be stored in the folder defined as “location” in the “document root” place. Type of document the directory is dedicated to. Unique Id for the directory. “Document root” is defined in the global parameters file. 