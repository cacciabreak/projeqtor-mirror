��          �               �   5   �      #  ,   +  3   X  <   �  D   �  F        U     \  7   d  Z   �  6   �  �  .  6   �     �  <   �  9   3  H   m  M   �  I        N     W  R   `  R   �  >      A list of report are available in different catagory. Buttons Click on a buttons to produce report. |four| Click on |buttonIconDisplay| to display the report. Click on |buttonIconPdf| to export the report as PDF format. Click on |buttonIconPrint| to get a printable version of the report. Click on |buttonIconToday| to display this report on the Today screen. Report Reports Select a category |one|, report list |two| will update. Select a report in the list, this will display specific parameters |three| for the report. Update the parameters to get the information you need. Project-Id-Version: ProjeQtOr User guide 5.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-15 13:02-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: fr
Language-Team: fr <LL@li.org>
Plural-Forms: nplurals=2; plural=(n > 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.5.3
 La liste des rapports sont disponibles par catégorie. Boutons Cliquer sur un des bouttons pour produire le rapport. |four| Cliquer sur |buttonIconDisplay| pour afficher le rapport. Cliquer sur |buttonIconPdf| pour exporter le rapport dans en format PDF. Cliquer sur |buttonIconPrint| pour obtenir une version imprimable du rapport. Cliquer sur |buttonIconToday| pour l'ajouter dans l'écran "Aujourd'hui". Rapports Rapports Sélectionnez une catégorie |one|, la liste des rapports sera mise à jour. |two| Sélectionnez un rapport dans la liste et ses parameters seront affichés. |three| Modifiez les paramètres pour obtenir l'information désirée. 