��          �      ,      �     �     �  #   �  #   �     �       #   *     N     e     w  "   �  (   �  !   �     �  2     A   7  �   y     ^  #   k  &   �  )   �     �     �  +        ;     N     `  (   n  -   �  0   �     �  5     V   H        
                                        	                              :ref:`photo` Changes the user's password. Field: Project indent char in lists Generic display parameter for user. Photo of the user. Section: Display parameters Section: Graphic interface behavior Section: Miscellaneous Section: Password Section: Photo Section: Print & Export parameters Selection of graphic interface behavior. Set to "none" to get a flat list. User parameters User parameters are efficient even without saving. User parameters screen allows configuration of personal settings. Project-Id-Version: 
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 8bit
Language: de
X-Generator: Poedit 2.0.8
 :ref:`photo` Ąndert das Passwort des Benutzers. Feld: Projekt-Einr�ckzeichen in Listen Generische Anzeigeparameter f�r Benutzer. Foto des Benutzers. Sektion: Parameter anzeige Sektion: Grafisches Schnittstellenverhalten Sektion: Sonstiges Sektion: Kennwort Sektion: Foto Sektion: Parameter drucken & exportieren Auswahl des grafischen Oberfl�chenverhaltens. Set to "none", um eine flache Liste zu bekommen. Benutzerdefinierte Paramete Benutzerparameter sind auch ohne Speichern effizient. Benutzerparameter-Bildschirm erlaubt die Konfiguration von pers�nlichen Einstellungen. 