# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2018, ProjeQtOr
# This file is distributed under the same license as the ProjeQtOr Bedienhandbuch Deutsch package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: ProjeQtOr Bedienhandbuch Deutsch 7.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-07-03 14:34+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../Order.rst:8
msgid "Quotation and Order"
msgstr ""

#: ../../Order.rst:17
msgid "Quotations"
msgstr ""

#: ../../Order.rst:19
msgid "A quotation is a proposal estimate sent to customer to get approval of what’s to be done, and how must the customer will pay for it."
msgstr ""

#: ../../Order.rst:21
msgid "On the quotation form, you can record all the information about the sent proposal, including attaching some file completely describing the proposal with details terms and conditions."
msgstr ""

#: ../../Order.rst:24
msgid "Transform quotation to order"
msgstr ""

#: ../../Order.rst:25
msgid "A quotation can be copied into an order when corresponding document is received as customer agreement."
msgstr ""

#: ../../Order.rst:28
#: ../../Order.rst:167
msgid "Bill lines section"
msgstr ""

#: ../../Order.rst:29
msgid "This section allows to detail the quotation modality."
msgstr ""

#: ../../Order.rst:34
#: ../../Order.rst:172
msgid ":ref:`Bill lines<manual-billing>`"
msgstr ""

#: ../../Order.rst:35
#: ../../Order.rst:173
msgid ":ref:`Linked element<linkElement-section>`"
msgstr ""

#: ../../Order.rst:36
#: ../../Order.rst:174
msgid ":ref:`Attachments<attachment-section>`"
msgstr ""

#: ../../Order.rst:37
#: ../../Order.rst:175
msgid ":ref:`Notes<note-section>`"
msgstr ""

#: ../../Order.rst:41
#: ../../Order.rst:178
msgid "Section: Description"
msgstr ""

#: ../../Order.rst:48
#: ../../Order.rst:83
#: ../../Order.rst:185
#: ../../Order.rst:226
msgid "Field"
msgstr ""

#: ../../Order.rst:49
#: ../../Order.rst:84
#: ../../Order.rst:186
#: ../../Order.rst:207
#: ../../Order.rst:227
msgid "Description"
msgstr ""

#: ../../Order.rst:50
#: ../../Order.rst:187
msgid ":term:`Id`"
msgstr ""

#: ../../Order.rst:51
msgid "Unique Id for the quotation."
msgstr ""

#: ../../Order.rst:52
#: ../../Order.rst:189
msgid "**Name**"
msgstr ""

#: ../../Order.rst:53
msgid "Short description of the quotation."
msgstr ""

#: ../../Order.rst:54
msgid "**Quotation type**"
msgstr ""

#: ../../Order.rst:55
msgid "Type of quotation."
msgstr ""

#: ../../Order.rst:56
msgid "**Project**"
msgstr ""

#: ../../Order.rst:57
msgid "The project concerned by the quotation."
msgstr ""

#: ../../Order.rst:58
#: ../../Order.rst:205
msgid ":term:`Origin`"
msgstr ""

#: ../../Order.rst:59
msgid "Element which is the origin of the quotation."
msgstr ""

#: ../../Order.rst:60
#: ../../Order.rst:195
msgid "Customer"
msgstr ""

#: ../../Order.rst:61
msgid "Customer concerned by the quotation."
msgstr ""

#: ../../Order.rst:62
#: ../../Order.rst:197
msgid "Contact"
msgstr ""

#: ../../Order.rst:63
msgid "Contact in customer organization to whom you sent the quotation."
msgstr ""

#: ../../Order.rst:64
msgid ":term:`Request<Description>`"
msgstr ""

#: ../../Order.rst:65
msgid "Request description."
msgstr ""

#: ../../Order.rst:66
#: ../../Order.rst:209
msgid "Additional info."
msgstr ""

#: ../../Order.rst:67
msgid "Any additional information about the quotation."
msgstr ""

#: ../../Order.rst:69
#: ../../Order.rst:118
#: ../../Order.rst:212
#: ../../Order.rst:258
msgid "**\\* Required field**"
msgstr ""

#: ../../Order.rst:73
#: ../../Order.rst:216
msgid "Automatically updated from project field."
msgstr ""

#: ../../Order.rst:76
#: ../../Order.rst:219
msgid "Section: Treatment"
msgstr ""

#: ../../Order.rst:85
#: ../../Order.rst:228
msgid "**Status**"
msgstr ""

#: ../../Order.rst:86
msgid "Actual :term:`status` of the quotation."
msgstr ""

#: ../../Order.rst:87
#: ../../Order.rst:230
msgid ":term:`Responsible`"
msgstr ""

#: ../../Order.rst:88
msgid "Resource who is responsible for the quotation."
msgstr ""

#: ../../Order.rst:89
msgid "Sent date"
msgstr ""

#: ../../Order.rst:90
msgid "Date when quotation is sent to customer contact."
msgstr ""

#: ../../Order.rst:91
msgid "Send mode"
msgstr ""

#: ../../Order.rst:92
#: ../../Order.rst:204
msgid "Delivery mode."
msgstr ""

#: ../../Order.rst:93
msgid "Offer validity"
msgstr ""

#: ../../Order.rst:94
msgid "Limit date of the validity of the proposal."
msgstr ""

#: ../../Order.rst:95
msgid "Likelihood"
msgstr ""

#: ../../Order.rst:96
msgid "The probability that the proposal will be accepted."
msgstr ""

#: ../../Order.rst:97
#: ../../Order.rst:232
msgid ":term:`Handled`"
msgstr ""

#: ../../Order.rst:98
msgid "Box checked indicates that the quotation is taken in charge."
msgstr ""

#: ../../Order.rst:99
#: ../../Order.rst:234
msgid ":term:`Done`"
msgstr ""

#: ../../Order.rst:100
msgid "Box checked indicates that the quotation is processed."
msgstr ""

#: ../../Order.rst:101
#: ../../Order.rst:236
msgid ":term:`Closed`"
msgstr ""

#: ../../Order.rst:102
msgid "Box checked indicates that the quotation is archived."
msgstr ""

#: ../../Order.rst:103
#: ../../Order.rst:238
msgid "Cancelled"
msgstr ""

#: ../../Order.rst:104
msgid "Box checked indicates that the quotation is cancelled."
msgstr ""

#: ../../Order.rst:105
msgid "Planned end date"
msgstr ""

#: ../../Order.rst:106
msgid "Target end date of the activity object of the quotation."
msgstr ""

#: ../../Order.rst:107
#: ../../Order.rst:138
#: ../../Order.rst:240
#: ../../Order.rst:287
msgid "Activity type"
msgstr ""

#: ../../Order.rst:108
msgid "Type of the activity object of the quotation."
msgstr ""

#: ../../Order.rst:109
msgid "Payment deadline"
msgstr ""

#: ../../Order.rst:110
msgid "The payment deadline is stated on the quotation."
msgstr ""

#: ../../Order.rst:111
msgid "Amount"
msgstr ""

#: ../../Order.rst:112
msgid "Total amount of the quotation."
msgstr ""

#: ../../Order.rst:113
msgid "Estimated work"
msgstr ""

#: ../../Order.rst:114
msgid "Work days corresponding to the quotation."
msgstr ""

#: ../../Order.rst:115
#: ../../Order.rst:254
msgid "Comments"
msgstr ""

#: ../../Order.rst:116
msgid "Comment about the treatment of the quotation."
msgstr ""

#: ../../Order.rst:122
msgid "If the payment deadline is not set, the value defined for the selected customer is used."
msgstr ""

#: ../../Order.rst:126
#: ../../Order.rst:262
msgid "Columns:"
msgstr ""

#: ../../Order.rst:128
msgid "**Ex VAT**: Amount without taxes."
msgstr ""

#: ../../Order.rst:130
msgid "The amount is automatically updated with the sum of bill lines."
msgstr ""

#: ../../Order.rst:132
#: ../../Order.rst:268
msgid "**Tax**: Applicable tax."
msgstr ""

#: ../../Order.rst:134
#: ../../Order.rst:270
msgid "If the applicable tax isn’t set, the tax defined for the selected customer is used."
msgstr ""

#: ../../Order.rst:136
#: ../../Order.rst:272
msgid "**Full**: Amount with taxes."
msgstr ""

#: ../../Order.rst:140
#: ../../Order.rst:289
msgid "The activity should be created only after approval."
msgstr ""

#: ../../Order.rst:154
msgid "Orders"
msgstr ""

#: ../../Order.rst:156
msgid "An order (also called command) is the trigger to start work."
msgstr ""

#: ../../Order.rst:158
msgid "On the order form, you can record all the information of the received order."
msgstr ""

#: ../../Order.rst:161
msgid "Scheduled work and budgeted cost of project"
msgstr ""

#: ../../Order.rst:162
msgid "The scheduled work (field: \"validated work\") of the project will be initialized with the sum of total work from all orders."
msgstr ""

#: ../../Order.rst:163
msgid "The budgeted cost (field: \"validated cost\") of the project will be initialized with the sum of the total amount before taxes for all orders."
msgstr ""

#: ../../Order.rst:164
msgid "See: :ref:`progress-section-resource`"
msgstr ""

#: ../../Order.rst:168
msgid "This section allows to detail the order modality."
msgstr ""

#: ../../Order.rst:188
msgid "Unique Id for the order."
msgstr ""

#: ../../Order.rst:190
msgid "Short description of the order."
msgstr ""

#: ../../Order.rst:191
msgid "**Order type**"
msgstr ""

#: ../../Order.rst:192
msgid "Type of order."
msgstr ""

#: ../../Order.rst:193
msgid "Project"
msgstr ""

#: ../../Order.rst:194
msgid "The project concerned by the order."
msgstr ""

#: ../../Order.rst:196
msgid "Customer concerned by the order."
msgstr ""

#: ../../Order.rst:198
msgid "Contact in customer organization to whom you sent the order."
msgstr ""

#: ../../Order.rst:199
msgid "**External reference**"
msgstr ""

#: ../../Order.rst:200
msgid ":term:`External reference` of the order (as received)."
msgstr ""

#: ../../Order.rst:201
msgid "Date of receipt"
msgstr ""

#: ../../Order.rst:202
msgid "Receipt date."
msgstr ""

#: ../../Order.rst:203
msgid "Receive mode"
msgstr ""

#: ../../Order.rst:206
msgid "Element which is the origin of the order."
msgstr ""

#: ../../Order.rst:208
msgid "Complete description of the order."
msgstr ""

#: ../../Order.rst:210
msgid "Any additional information about the order."
msgstr ""

#: ../../Order.rst:229
msgid "Actual :term:`status` of the order."
msgstr ""

#: ../../Order.rst:231
msgid "Resource who is responsible for the order."
msgstr ""

#: ../../Order.rst:233
msgid "Box checked indicates that the order is taken in charge."
msgstr ""

#: ../../Order.rst:235
msgid "Box checked indicates that the order is processed."
msgstr ""

#: ../../Order.rst:237
msgid "Box checked indicates that the order is archived."
msgstr ""

#: ../../Order.rst:239
msgid "Box checked indicates that the order is cancelled."
msgstr ""

#: ../../Order.rst:241
msgid "Type of the activity object of the order."
msgstr ""

#: ../../Order.rst:242
msgid "Linked activity"
msgstr ""

#: ../../Order.rst:243
msgid "Activity representing the execution of the order."
msgstr ""

#: ../../Order.rst:244
msgid "Initial"
msgstr ""

#: ../../Order.rst:245
msgid "Initial values."
msgstr ""

#: ../../Order.rst:246
msgid "Amendment"
msgstr ""

#: ../../Order.rst:247
msgid "Additional values."
msgstr ""

#: ../../Order.rst:248
msgid "Total"
msgstr ""

#: ../../Order.rst:249
msgid "Sum of the initial values and amendment."
msgstr ""

#: ../../Order.rst:250
msgid "Start date"
msgstr ""

#: ../../Order.rst:251
msgid "Initial start date of the execution of the order."
msgstr ""

#: ../../Order.rst:252
msgid "End date"
msgstr ""

#: ../../Order.rst:253
msgid "Initial and validated end date of the execution of the order."
msgstr ""

#: ../../Order.rst:255
msgid "Comment about the treatment of the order."
msgstr ""

#: ../../Order.rst:264
msgid "**Ex VAT**: Amount before taxes."
msgstr ""

#: ../../Order.rst:266
msgid "The column value is automatically updated with the sum of bill line amounts."
msgstr ""

#: ../../Order.rst:273
msgid "**Work**: Work days corresponding to the order."
msgstr ""

#: ../../Order.rst:275
msgid "The column value is automatically updated with the sum of bill lines quantities."
msgstr ""

#: ../../Order.rst:276
msgid "When the measure unit is \"day\"."
msgstr ""

#: ../../Order.rst:280
msgid "The columns values \"Ex VAT\" and \"Work\" are automatically updated with the sum of billing lines with selected amendment checkboxes."
msgstr ""

#: ../../Order.rst:284
msgid "**Initial** : Initial dates"
msgstr ""

#: ../../Order.rst:285
msgid "**Validated** : Validated dates"
msgstr ""

