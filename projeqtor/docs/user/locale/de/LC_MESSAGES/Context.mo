��          �               �     �     �     �  
   �     �  ?   �          +  c   4     �     �     �  .   �     �  .   �     )     H     Y  
   n  f   y     �  9   �     -  n  H     �     �     �  
   �     �  Z   �  
   U     `     i     �     �     �  :        ?  ;   R     �     �     �     �  �   �     q  7   �     �   **Name** **\* Required field** :term:`Closed` :term:`Id` Browser Captions are translated and so can be changed in language file. Context type Contexts Contexts are initially set to be able to define contexts for IT Projects, for three context types : Description Environment Field Flag to indicate that the context is archived. Name of the context. Number to define the order of display in lists One of the three context type. Operating System Section: Description Sort order The contexts defines a list of elements selectable to define ticket context and test case environment. The list is fixed. They can be changed to be adapted to any kind of project. Unique Id for the context. Project-Id-Version: ProjeQtOr Bedienhandbuch Deutsch 7.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-07-03 14:34+0200
PO-Revision-Date: 2018-07-19 14:38+0200
Last-Translator: 
Language: de
Language-Team: 
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 **Name** **\* Pflichtfeld** :term:`Geschlossen` :term:`Id` Browser Bildunterschriften werden übersetzt und können so in einer Sprachdatei geändert werden. Kontexttyp Kontexte Kontexte sind zunächst so eingestellt, dass sie Kontexte für IT-Projekte definieren können, und zwar für drei Kontexttypen: Beschreibung Umgebung Feld Kennzeichen, das angibt, dass der Kontext archiviert wird. Name des Kontexts. Nummer zur Festlegung der Reihenfolge der Anzeige in Listen Eines der drei Kontexttypen. Betriebssystem Abschnitt: Beschreibung Sortierreihenfolge Der Kontext definiert eine Liste von Elementen, die zur Definition des Ticketkontextes und der Testfallumgebung ausgewählt werden können. Die Liste ist festgelegt. Sie können für jede Art von Projekt angepasst werden. Eindeutige Id für den Kontext. 