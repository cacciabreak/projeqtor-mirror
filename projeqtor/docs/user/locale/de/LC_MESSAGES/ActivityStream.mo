��          �               �      �      �   5     7   8  7   p  )   �  G   �  -     '   H  (   p  9   �  R   �  n  &     �     �  E   �  G   �  E   9  4     k   �  7      +   X  7   �  C   �  \       Activity Stream Chat Click on |buttonCollapseClose|  to hide note comment. Click on |buttonCollapseOpen|  to display note comment. Click on |iconHideStream|  to hide or display the chat. Display of notes on right part of screen. Possibility to quickly add note, write your text and press 'Enter' key. The chat displays notes on the selected item. There are filters to refine the search. This screen is devoted to display notes. To default you will see all visible notes for each items. You can change the visibility of the note if you click on the bottom right corner. Project-Id-Version: ProjeQtOr Bedienhandbuch Deutsch 7.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-07-03 14:34+0200
PO-Revision-Date: 2018-07-16 12:39+0200
Last-Translator: 
Language: de
Language-Team: 
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 Aktivitätsfluss Chat Klicken Sie auf |buttonCollapseClose|, um den Kommentar auszublenden. Klicken Sie auf |buttonCollapseOpen|, um den Notizkommentar anzuzeigen. Klicken Sie auf |iconHideStream|, um den Chat ein- oder auszublenden. Anzeige der Notizen im rechten Teil des Bildschirms. Möglichkeit, schnell Notizen hinzuzufügen, Ihren Text zu schreiben und die Taste „Enter“ zu drücken. Der Chat zeigt Notizen zu dem ausgewählten Element an. Es gibt Filter, um die Suche zu verfeinern. Dieser Bildschirm ist der Anzeige von Notizen gewidmet. Standardmäßig sehen Sie alle sichtbaren Notizen zu jedem Element. Sie können die Sichtbarkeit der Notiz ändern, wenn Sie auf die untere rechte Ecke klicken. 