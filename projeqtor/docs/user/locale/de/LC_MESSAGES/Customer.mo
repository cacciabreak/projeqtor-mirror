��          �               �     �     �  
   �     �     
  '        ?     O     e  -   {  3   �  @   �  (     '   G     o  6   �  8   �  1   �     #      )  *   J     u  0   �  *   �  /   �  n       �     �     �  
   �     �  +   �               6  G   K  D   �  ]   �  2   6  1   i     �  U   �  J   	  H   L	     �	  +   �	  C   �	     
  7   0
  >   h
  5   �
   **Grey days** : Days off **Yellow day** : Current day 1 - Period 2 - 1st day 3 - Resource 4 - Show done items & Show closed items 5 - Top buttons 6 - Left side buttons 7 - Day number button Allows to change current month, week, or day. Allows to display or not the done and closed items. Allows to display planned task to a resource on a calendar view. Allows to select the displayed calendar. Allows to select the resource calendar. Calendar selector Click on the day number button to go day display mode. Click on |arrowLeft| to return to the last display mode. Click on |arrowRight| to go to week display mode. Diary Display the month, week, or day. Just click on any task to access directly. On mouse over the task The first day of mouth or the week is displayed. This view can be monthly, weekly or daily. You can see a short information about the task. Project-Id-Version: ProjeQtOr Bedienhandbuch Deutsch 7.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-07-03 14:34+0200
PO-Revision-Date: 2018-07-09 14:48+0200
Last-Translator: 
Language: de
Language-Team: 
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 ** Graue Tage **: Ruhetage **Gelber Tag*** Aktueller Tag 1 - Periode 2 - 1. Tag 3 - Rohstoff 4 - Fertige & Geschlossene Artikel anzeigen 5 - Top-Tasten 6 - Linke Seitentasten 7 - Tageszahlentaste Ermöglicht das Ändern des aktuellen Monats, der Woche oder des Tages. Ermöglicht die Anzeige der erledigten und abgeschlossenen Aufgaben. Ermöglicht die Anzeige einer geplanten Aufgabe für eine Ressource in einer Kalenderansicht. Ermöglicht die Auswahl des angezeigten Kalenders. Ermöglicht die Auswahl des Ressourcen Kalenders. Kalenderauswahl Klicken Sie auf die Schaltfläche Tageszahl, um in den Tagesanzeigemodus zu gelangen. Klicken Sie auf | PfeilLeft | um zum letzten Anzeigemodus zurückzukehren. Klicken Sie auf | arrowRight | um in den Wochenanzeigemodus zu gelangen. Bautagebuch Zeigt den Monat, die Woche oder den Tag an. Klicken Sie einfach auf eine Aufgabe, um direkt darauf zuzugreifen. Mit der Maus über der Aufgabe Der erste Tag des Monats oder der Woche wird angezeigt. Diese Ansicht kann monatlich, wöchentlich oder täglich sein. Sie können eine kurze Information zur Aufgabe sehen. 