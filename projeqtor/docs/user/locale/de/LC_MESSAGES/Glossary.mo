��    (      \  5   �      p  6   q     �     �     �     �  >   �     3  !   ?     a  @   f     �  )   �  7   �  '        D     Q     Z  <   b     �  5   �  =   �  G     5   ^  G   �  5   �  +     *   >     i  	   p  E   z     �     �  .   �  .   �  "   ,  5   O  5   �     �     �  �   �  A   �	      
     
  *    
  '   K
  :   s
     �
     �
     �
  X   �
     <  3   M  ?   �  ;   �     �       
     <        [  9   ^  K   �  ]   �  E   B  G   �  7   �  1     *   :     e     n  Q   w     �     �  6   �  @     '   Q  J   y  B   �                     #   !                              	                   &                        %   
             $                      '         "              (                            A result is a description of the treatment of an item. Accelerator button Closed Date of done is saved. Date of handling is saved. Depending on the element type, name of field can be different. Description Determines the element of origin. Done Every item has a unique Id, automatically generated on creation. External reference Flag to indicate that item has been done. Flag to indicate that item has been taken into account. Flag to indicate that item is archived. GUI behavior Glossary Handled Hierarchical position of the element in the global planning. Id It defines the progress of the treatment of the item. It is possible to define that description field is mandatory. It is possible to define that result field is mandatory on done status. It uses to refer information from an external source. Item is linked to a element type, element type is linked to a workflow. More detail, see : :ref:`Origin field<origin-field>`. More detail, see: **Administration guide**. More detail, see: :ref:`behavior-section`. Origin Reference Reference is displayed after id, automatically generated on creation. Result Status The status determines the life cycle of items. This button allows to skip to the next status. This field allows fill free input. This field allows to define a description on an item. This generally means that responsible has been named. WBS Work Breakdown Structure. Project-Id-Version: 
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 8bit
Language: de
X-Generator: Poedit 2.0.8
 Ein Ergebnis ist eine Beschreibung der Behandlung eines Artikels. Beschleuniger-Taste Geschlossen Datum der Fertigstellung wird gespeichert. Datum der Bearbeitung wird gespeichert. Je nach Elementtyp kann der Feldname unterschiedlich sein. Beschreibung Bestimmt das Ursprungselement. Erledigt Jeder Artikel hat eine eindeutige ID, die bei der Erstellung automatisch generiert wird. Externe Referenz Flagge, um anzuzeigen, dass der Artikel fertig ist. Kennzeichen, das angibt, dass der Artikel ber�cksichtigt wurde. Kennzeichen, um anzuzeigen, dass das Objekt archiviert ist. GUI-Verhalten Glossar Bearbeitet Hierarchische Position des Elements in der globalen Planung. Id Es definiert den Fortschritt der Behandlung des Artikels. Es ist m�glich zu definieren, dass das Beschreibungsfeld obligatorisch ist. Es ist m�glich, zu definieren, dass das Ergebnisfeld bei erledigtem Status obligatorisch ist. Es verwendet, um Informationen von einer externen Quelle zu beziehen. Item ist mit einer Elementart verkn�pft, Elementart mit einem Workflow. Mehr Details, siehe: :ref:`Origin field<origin-field>`. Mehr Details, siehe: **Administrationshandbuch**. Mehr dazu, siehe: :ref:`behavior-section`. Herkunft Referenz Referenz wird nach der ID angezeigt, die beim Anlegen automatisch generiert wird. Ergebnis Status Der Status bestimmt den Lebenszyklus von Gegenst�nden. Mit dieser Schaltfl�che k�nnen Sie zum n�chsten Status springen. Dieses Feld erlaubt eine freie Eingabe. Dieses Feld erlaubt es, eine Beschreibung f�r einen Artikel zu definieren. Das bedeutet in der Regel, dass der Verantwortliche genannt wurde. PSP Projektstrukturplan. 