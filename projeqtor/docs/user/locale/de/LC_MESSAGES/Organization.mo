��          �   %   �      @     A     J     `  &   v  1   �  *   �     �  
     3         T     [     g  	   m     w     �     �     �     �     �     �          !     A     `  �   u     Z     c     x  &   �  1   �  *   �          *  @   6     w     �     �  
   �     �     �     �     �     �               3     I     i     �     
                                                                                                   	                      **Name** **Organization type** **\* Required field** :ref:`Attachments<attachment-section>` :ref:`Current project<progress-section-activity>` :ref:`Linked element<linkElement-section>` :ref:`Notes<note-section>` :term:`Id` Box checked indicates the organization is archived. Closed Description Field Hierarchy Management of organizations Manager Manager of organization. Organizations Parent organization Section: Description Short name of the Organization. Type of organization. Unique Id for the Organization. list of parents organizations. parent organization. Project-Id-Version: 
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 8bit
Language: de
X-Generator: Poedit 2.0.8
 **Name** **Organisationsart** **\* Pflichtfeld** :ref:`Attachments<Attachment-Bereich>` :ref:`Current project<progress-section-activity>` :ref:`Linked element<linkElement-sektion>` :ref:`Notes<note-section>` :term:`Id`` Kontrollk�stchen zeigt an, dass die Organisation archiviert ist. Geschlossen Beschreibung Feld Hierarchie Management von Organisationen Manager Manager der Organisation. Organisationen Muttergesellschaft Sektion: Beschreibung Kurzname der Organisation. Art der Organisation. Unique Id for the Organization. Liste der Elternorganisationen. Mutterorganisation. 