��    '      T              �     �     �     �  
   �     �     �     �  
   �     �  ,        9     G     T     b  #   n     �  J   �  0   �  3        H     g          �     �  "   �     �  #     
   '  ;   2     n       *   �     �     �  
   �  2   �  2   #     V  n  s     �     �        
             8     =  	   M     W  9   j     �     �     �     �  (   �     	  b   	  =   {	  <   �	  &   �	     
     7
     T
  (   h
  .   �
     �
  *   �
  
     [        h     }  )   �     �  
   �     �  ;   �  ;   -  "   i   **Name** **\* Required field** :term:`Closed` :term:`Id` BIC for the recipient. Bank Bank code (BIC) Bank name. Company number Company number, to be displayed on the bill. Contact email Contact name Contact phone Description Email of contact for the recipient. Field Flag to indicate that tax is automatically set to zero for this recipient. Flag to indicate that the recipient is archived. Full account number defining the BBAN account code. Full address of the recipient. IBAN for the recipient. International number (IBAN) Legal notice Legal notice for the recipient. Name of contact for the recipient. National account number Phone of contact for the recipient. Recipients Recipients are mainly defined to store billing information. Section: Address Section: Description Section: International Bank Account Number Short name of the recipient. Tax free Tax number Tax reference number, to be displayed on the bill. The recipient is the beneficiary of bill payments. Unique Id for the recipient. Project-Id-Version: ProjeQtOr Bedienhandbuch Deutsch 7.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-07-03 14:34+0200
PO-Revision-Date: 2018-07-19 15:33+0200
Last-Translator: 
Language: de
Language-Team: 
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 **Name** **\* Pflichtfeld *** :term:`Geschlossen` :term:`Id` BIC für den Empfänger. Bank Bank code (BIC) Bankname. Unternehmensnummer Firmennummer, die auf der Rechnung angezeigt werden soll. Kontakt-E-Mail Name des Ansprechpartners Kontakt-Telefon Beschreibung E-Mail des Kontakts für den Empfänger. Feld Kennzeichen, das angibt, dass die Steuer für diesen Empfänger automatisch auf Null gesetzt wird. Kennzeichen, das angibt, dass der Empfänger archiviert wird. Vollständige Kontonummer, die den BBAN-Kontocode definiert. Vollständige Adresse des Empfängers. IBAN für den Empfänger. Internationale Nummer (IBAN) Rechtliche Hinweise Rechtliche Hinweise für den Empfänger. Name des Ansprechpartners für den Empfänger. Nationale Kontonummer Telefonischer Kontakt für den Empfänger. Empfänger Empfänger werden hauptsächlich für die Speicherung von Rechnungsinformationen definiert. Abschnitt: Anschrift Abschnitt: Beschreibung Abschnitt: Internationale Bankkontonummer Kurzname des Empfängers. Steuerfrei Steuernummer Steuernummer, die auf der Rechnung ausgewiesen werden soll. Der Empfänger ist der Begünstigte von Rechnungszahlungen. Eindeutige Id für den Empfänger. 