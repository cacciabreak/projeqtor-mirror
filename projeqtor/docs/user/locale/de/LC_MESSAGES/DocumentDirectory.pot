# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2018, ProjeQtOr
# This file is distributed under the same license as the ProjeQtOr Bedienhandbuch Deutsch package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: ProjeQtOr Bedienhandbuch Deutsch 7.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-07-03 14:34+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../DocumentDirectory.rst:12
msgid "Document directories"
msgstr ""

#: ../../DocumentDirectory.rst:14
msgid "Document directories management allows to define a structure for document storage."
msgstr ""

#: ../../DocumentDirectory.rst:16
msgid "The files of document will be stored in the folder defined by the parameters  «Document root» and «Location»."
msgstr ""

#: ../../DocumentDirectory.rst:17
msgid "«Document root» is defined in :ref:`Global parameters <document-section>` screen."
msgstr ""

#: ../../DocumentDirectory.rst:20
msgid "Section: Description"
msgstr ""

#: ../../DocumentDirectory.rst:27
msgid "Field"
msgstr ""

#: ../../DocumentDirectory.rst:28
msgid "Description"
msgstr ""

#: ../../DocumentDirectory.rst:29
msgid ":term:`Id`"
msgstr ""

#: ../../DocumentDirectory.rst:30
msgid "Unique Id for the directory."
msgstr ""

#: ../../DocumentDirectory.rst:31
msgid "**Name**"
msgstr ""

#: ../../DocumentDirectory.rst:32
msgid "Name of the directory."
msgstr ""

#: ../../DocumentDirectory.rst:33
msgid "Parent directory"
msgstr ""

#: ../../DocumentDirectory.rst:34
msgid "Name of the parent directory to define hierarchic structure."
msgstr ""

#: ../../DocumentDirectory.rst:35
msgid "Location"
msgstr ""

#: ../../DocumentDirectory.rst:36
msgid "Folder where files will be stored."
msgstr ""

#: ../../DocumentDirectory.rst:37
msgid "Project"
msgstr ""

#: ../../DocumentDirectory.rst:38
msgid "Directory is dedicated to this project."
msgstr ""

#: ../../DocumentDirectory.rst:39
msgid "Product"
msgstr ""

#: ../../DocumentDirectory.rst:40
msgid "Directory is dedicated to this product."
msgstr ""

#: ../../DocumentDirectory.rst:41
msgid "Default type"
msgstr ""

#: ../../DocumentDirectory.rst:42
msgid "Type of document the directory is dedicated to."
msgstr ""

#: ../../DocumentDirectory.rst:43
msgid ":term:`Closed`"
msgstr ""

#: ../../DocumentDirectory.rst:44
msgid "Flag to indicate that directory is archived."
msgstr ""

#: ../../DocumentDirectory.rst:46
msgid "**\\* Required field**"
msgstr ""

#: ../../DocumentDirectory.rst:50
msgid "The current directory is then a sub-directory of parent."
msgstr ""

#: ../../DocumentDirectory.rst:54
msgid "Location is automatically defined as «Parent directory» / «Name»."
msgstr ""

#: ../../DocumentDirectory.rst:58
msgid "This project will be the default to new stored documents in this directory."
msgstr ""

#: ../../DocumentDirectory.rst:62
msgid "This product will be the default to new stored documents in this directory."
msgstr ""

#: ../../DocumentDirectory.rst:63
msgid "If the project is specified, the list of values contains the products linked the selected project."
msgstr ""

#: ../../DocumentDirectory.rst:64
msgid "If the project is not specified, the list of values contains all products defined."
msgstr ""

#: ../../DocumentDirectory.rst:68
msgid "This document  type  will be the default to new stored documents in this directory."
msgstr ""

