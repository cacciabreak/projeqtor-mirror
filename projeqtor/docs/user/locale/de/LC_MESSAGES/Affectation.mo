��    #      4              L     M     Y     e     {     �  "   �     �     �  
   �  $        )  (   5  '   ^     �     �     �     �  1   �  ]   �  M   I     �     �  #   �  
   �                     )     >  
   P     [     u  #   �     �  n  �  
   ;     F     R     e     �  "   �     �     �  
   �  $   �       7   %  )   ]     �  	   �     �     �  0   �  q   �  j   _	     �	      �	  .   
     :
     G
     ^
  	   g
     q
     �
  
   �
     �
  !   �
  9   �
         **Profile** **Project** **\* Required field** :ref:`allocation-to-project` :ref:`profiles-definition` :ref:`user-ress-contact-demystify` :term:`Closed` :term:`Description` :term:`Id` Allocation rate for the project (%). Allocations Allows to manage allocations to project. Complete description of the allocation. Description End date End date of allocation. Field Flag to indicate that the allocation is archived. If a contact is a resource and inversely, then resource or contact name will be selected too. If none is selected then the user connected is used to define the allocation. Name of the allocated contact. Name of the allocated resource. Offers a global view of allocation. Or contact Project allocated to. Rate Resource Section: Description Selected profile. Start date Start date of allocation. Unique Id for the allocation. You can select resource or contact. You can use filters. Project-Id-Version: ProjeQtOr Bedienhandbuch Deutsch 7.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-07-03 14:34+0200
PO-Revision-Date: 2018-07-17 14:46+0200
Last-Translator: 
Language: de
Language-Team: 
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 **Profil** **Projekt** **\* Pflichtfeld** :ref:`allocation-to-project` :ref:`profiles-definition` :ref:`user-ress-contact-demystify` :term:`Geschlossen` :term:`Beschreibung` :term:`Id` Zuordnungsrate für das Projekt (%). Zuordnungen Ermöglicht die Verwaltung von Zuordnungen zum Projekt. Vollständige Beschreibung der Zuordnung. Beschreibung Endedatum Endedatum der Zuordnung. Feld Kennzeichen, dass die Zuordnung archiviert wird. Wenn ein Kontakt eine Ressource ist und umgekehrt, dann wird auch die Ressource oder der Kontaktname ausgewählt. Wenn „keine“ ausgewählt ist, wird der angeschlossene Benutzer zur Definition der Zuordnung verwendet. Name des zugeordneten Kontakts. Name der zugeordneten Ressource. Bietet eine globale Sicht auf die Zuordnungen. Oder Kontakt Projekt zugeordnet zu. Bewerten Ressource Abschnitt: Beschreibung Ausgewähltes Profil. Startdatum Startdatum der Zuordnung. Eindeutige Id für die Zuordnung. Sie können eine Ressource oder einen Kontakt auswählen. Sie können Filter verwenden. 