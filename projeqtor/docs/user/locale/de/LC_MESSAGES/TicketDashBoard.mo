��    "      ,  /   <      �     �               .     D     \     o     �     �     �  1   �  :   �  ,   $  4   Q  $   �     �  %   �  3   �          '  
   ?     J     [     w  C   �  %   �     �  (     (   *  5   S      �  %   �  %   �  �   �     �     �       !        ;     V     l     �     �     �  N   �  >   	  1   F	  H   x	  *   �	     �	  4   �	  /   /
     _
     z
  	   �
     �
     �
     �
  S   �
  0   1     b  4   t  *   �  6   �  &     1   2  4   d                     !                                                                             	                "                                        
    **Added recently** **All issues** **Assigned to me** **Not closed issues** **Not resolved issues** **Reported by me** **Resolved recently** **Unscheduled** **Updated recently** All tickets. Allows to define reports displayed on the screen. Allows user to have a tickets global view of his projects. Arrange reports on left and right on screen. Click on |buttonIconParameter| to access parameters. Direct access to the list of tickets Filter clauses Filters are available to limit scope. For this report, filter clauses are not applicable. Linked to the user No resolution scheduled Parameters Recently updated Report: Synthesis by status Scope filters Shows several small reports, listing the number of tickets by item. Tickets created within *x* last days. Tickets dashboard Tickets not closed. (Status <> 'closed') Tickets not resolved. (Status <> 'done') Tickets that you are responsible for their treatment. Tickets that you are the issuer. Tickets treated within *x* last days. Tickets updated within *x* last days. Project-Id-Version: 
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=ISO-8859-1
Content-Transfer-Encoding: 8bit
Language: de
X-Generator: Poedit 2.0.8
 **K�rzlich hinzugef�gt** **Alle Ausgaben** **Assigned to me** **Nicht abgeschlossene Ausgaben** **Nicht gel�ste Probleme** **Berichtet von mir** **K�rzlich aufgel�st** **Ungeplant** **K�rzlich aktualisiert** Alle Karten. Erlaubt die Definition von Berichten, die auf dem Bildschirm angezeigt werden. Erm�glicht dem Benutzer eine globale Sicht auf seine Projekte. Berichte links und rechts am Bildschirm anordnen. Klicken Sie auf |buttonIconParameter|, um auf die Parameter zuzugreifen. Direkter Zugriff auf die Liste der Tickets Filterklausel Filter sind verf�gbar, um den Umfang einzuschr�nken. F�r diesen Bericht gelten keine Filterklauseln. Mit dem Benutzer verbunden Keine Aufl�sung geplant Parameter K�rzlich aktualisiert Bericht: Synthese nach Status Scope-Filter Zeigt mehrere kleine Berichte an, die die Anzahl der Tickets pro Artikel auflisten. Tickets erstellt innerhalb der letzten *x* Tage. Tickets Dashboard Tickets nicht geschlossen. (Status <> 'geschlossen') Tickets nicht gel�st. (Status <> 'fertig') Tickets, f�r deren Behandlung Sie verantwortlich sind. Tickets, dass Sie der Aussteller sind. Tickets behandelt innerhalb der letzten *x* Tage. Tickets aktualisiert innerhalb *x* der letzten Tage. 