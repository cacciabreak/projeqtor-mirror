<?php
// =======================================================================================
// Automatically generated parameter file
// on 2020-01-29 13:57:40
// =======================================================================================
$paramDbType='mysql'; $paramDbPort='3306'; $paramDbUser='root'; $paramDbPassword='mysql';
$paramDbName='projeqtor_v84';$paramDbPrefix='';
$paramDbHost='127.0.0.1';
$logFile='../files/logs/projeqtor_${date}.log';
$logLevel='3';
$enforceUTF8='1';
$paramSupportEmail="support@projeqtor.org";
$pdfNamePrefix="ProjeQtOr - ";
$debugQuery=false;
$debugJsonQuery=false;
$debugPerf=true;
$debugTraceUpdates=false;
$debugTraceHistory=false;           // Will add trace on each save() or delete() of History and Audit (only if $debugTraceUpdates=true;
$debugReport=true;
$i18nNocache=true;
$debugIEcompatibility=false;
$scaytAutoStartup='NO';
$dataCloningDirectory="D:\www\simulation";
$dataCloningUrl="http://localhost/simulation/";
$pathToWkHtmlToPdf="C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe";
//======= END